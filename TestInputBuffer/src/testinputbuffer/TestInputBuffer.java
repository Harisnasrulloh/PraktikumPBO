package testinputbuffer;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TestInputBuffer {
    public static void main(String[] args) {
        System.out.print("Masukkan angka : ");
        int i = Input.bacaData();
        System.out.println("Angka yang Anda masukkan adalah " + i);
    }
}
class Input {
    public static int bacaData() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int angka = 0;
        try {
            String input = br.readLine();
            angka = Integer.parseInt(input);
        } catch (Exception ex) {
            System.out.println("Input salah. Inisialisasi dg nilai " + angka);
        } finally {
            return angka;
        }
    }
}