package com.rental;

public class Bus {
    int day;
    int jenis;
    int harga;

    public Bus (int day,int jenis){
        this.day = day;
        this.jenis = jenis;
    }
    public int getHarga(int day,int jenis){
        if (jenis ==1){
            if (day<=3){
                harga = day*1300000;
            }
            else harga = day*1000000;
        }
        else if (jenis==2){
            if (day<=3){
                harga = day*1800000;
            }
            else harga = day*1500000;
        }
        else if (jenis==3){
            if (day<=3){
                harga = day *2300000;
            }
            else harga = day*2100000;
        }
        return harga;
    }
}
