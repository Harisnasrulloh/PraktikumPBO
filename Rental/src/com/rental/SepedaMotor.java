package com.rental;

public class SepedaMotor {
    int jam;
    int day;
    int harga;
    public SepedaMotor(int jam,int day){
        this.jam = jam;
        this.day = day;
    }
    public int hargajam(int jam){
        if (jam <=6){
            harga = jam*5000;
        }
        else if (jam < 12){
            harga = jam*4000;
        }
        else harga = jam*3000;
        return harga;
    }
    public int hargaday(int day){
        if (day >1 && day<=7){
            harga = day*30000;
        }
        else {
            harga = day*20000;
        }
        return harga;
    }
}
