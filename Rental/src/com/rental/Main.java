package com.rental;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        char ulang;
        int menu;
        Scanner input = new Scanner(System.in);
        do {
            System.out.println("Menu Rental");
            System.out.println("1. Sepeda Motor");
            System.out.println("2. Mobil");
            System.out.println("3. Bis Pariwisata");
            System.out.print("Pilih : ");
            menu = input.nextInt();
            switch (menu){
                case 1 :
                    int jamsm = 0;
                    System.out.println("Sewa Sepeda Motor");
                    System.out.print("Berapa hari : ");
                    int timesm = input.nextInt();
                    SepedaMotor motor = new SepedaMotor(jamsm,timesm);
                    if (timesm == 1){
                        System.out.print("Berapa jam : ");
                        jamsm = input.nextInt();
                        if (jamsm > 0 && jamsm <24){
                            System.out.println("Harga Sewa = Rp. "+motor.hargajam(jamsm));
                        }
                    }
                    else if (timesm >1){
                        System.out.println("Harga Sewa = Rp. "+motor.hargaday(timesm));
                    }
                    else {
                        System.out.println("Input error");
                    }

                    break;

                case 2 :
                    System.out.println("Sewa Mobil");
                    System.out.print("Berapa hari : ");
                    int timemobil = input.nextInt();
                    Mobil mobil = new Mobil(timemobil);
                    if (timemobil > 0){
                        System.out.println("Harga Sewa = Rp. "+mobil.getHarga(timemobil));
                    }
                    else {
                        System.out.println("Input Error");
                    }
                    break;
                case 3 :
                    System.out.println("Sewa Bis Pariwisata");
                    System.out.println("1. Bis Elf");
                    System.out.println("2. Bis Medium");
                    System.out.println("3. Bis Pariwisata");
                    System.out.print("Pilih : ");
                    int pilihbus = input.nextInt();
                    System.out.print("Berapa hari : ");
                    int timebus = input.nextInt();
                    Bus bus = new Bus(timebus,pilihbus);
                    if (timebus > 0 && pilihbus >0 && pilihbus <= 3){
                        System.out.println("Harga Sewa = Rp. "+bus.getHarga(timebus,pilihbus));
                    }
                    else{
                        System.out.println("Input Error");
                    }
                    break;
                default:
                    System.out.println("Pilih antara 1 sampai 3");
                    break;
            }
            System.out.print("Kembali ke menu ? ");
            ulang = input.next().charAt(0);
        }while (ulang=='y'||ulang=='Y');
    }
}
