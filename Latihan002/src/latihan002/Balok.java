package latihan002;
public class Balok extends PersegiPanjang{

    Balok(double panjang,double lebar,double tinggi){
        super(panjang, lebar, tinggi);
    }
    public double luasPB(double panjang,double lebar,double tinggi){
        luas = (2*panjang*tinggi)+(2*panjang*lebar)+(2*lebar*tinggi);
        return luas;
    }
    public double vbalok(){
        volume = panjang*lebar*tinggi;
        return volume;
    }
}
