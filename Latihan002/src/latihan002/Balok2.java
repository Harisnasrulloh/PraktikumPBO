package latihan002;
public class Balok2 extends PersegiPanjang{
    double lplimas,vlimas;
    public Balok2(double panjang,double lebar,double tinggi) {
        super(panjang, lebar, tinggi);
    }
    public double vbalok2(double panjang,double lebar,double tinggi){
        volume = panjang*lebar*tinggi;
        return volume;
    }
    public double lpLimas(){
        lplimas = panjang;
        return lplimas;
    }
    public double vlimas(){
        vlimas = volume/3;
        return vlimas;
    }
}
