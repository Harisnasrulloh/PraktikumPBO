package latihan001;

import java.util.Scanner;
public class Latihan001 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        final double pi = 3.14;
//        System.out.print("Nama : ");
//        String nama = input.nextLine();
//        System.out.print("Jumlah anak ayam  = ");
//        int jumlah = input.nextInt();
//        System.out.println("Anak ayam pak "+nama+" ada : "+jumlah);
//        for (int i = jumlah; i >=1; i--) {
//            int sisa = i - 1;
//            System.out.println("Anak ayam turun "+i);
//            if (sisa == 0) {
//                System.out.println("Mati satu pak "+nama+" galau");
//            }
//            else{
//                System.out.println("Mati satu tinggal "+sisa);
//            }
//        }
        char ulang;
        do{
        System.out.println("Menu : ");
        System.out.println("1. Luas");
        System.out.println("2. Volume");
        System.out.print("Pilih menu : ");
        int menu = input.nextInt();
            if (menu == 1) {
                System.out.println("Cari Luas");
                System.out.println("1. Segitiga");
                System.out.println("2. Persegi Panjang");
                System.out.print("Pilih = ");
                int luas = input.nextInt();
                if (luas == 1) {
                    System.out.print("Alas = ");
                    int alas = input.nextInt();
                    System.out.print("Tinggi = ");
                    int tinggi = input.nextInt();
                    double lsegitiga = alas*tinggi/2;
                    System.out.println("Luas Segitiga adalah "+lsegitiga);
                }
                else{
                    System.out.print("Panjang = ");
                    int panjang = input.nextInt();
                    System.out.print("Lebar = ");
                    int lebar = input.nextInt();
                    int lpp = panjang*lebar;
                    System.out.println("Luas Persegi Panjangnya adalah "+lpp);
                }
            }
            else if(menu == 2){
                System.out.println("Cari Volume");
                System.out.println("1. Tabung");
                System.out.println("2. Limas Segi Empat");
                System.out.print("Pilih = ");
                int volume = input.nextInt();
                if (volume==1) {
                    System.out.print("Jari - jari = ");
                    int jari = input.nextInt();
                    System.out.print("Tinggi = ");
                    int tinggi = input.nextInt();
                    double vtabung = jari*jari*tinggi*pi;
                    System.out.println("Volume Tabungnya adalah "+vtabung);
                }
                else{
                    System.out.print("Sisi = ");
                    int sisi = input.nextInt();
                    System.out.print("Tinggi = ");
                    int tinggi = input.nextInt();
                    int lp = sisi*sisi;
                    double vlimaspersegi = lp*tinggi/3;
                    System.out.println("Volume Limas Persegi adalah "+vlimaspersegi);
                }
            }
            System.out.print("Ulang ? ");
            ulang = input.next().charAt(0);
        } while (ulang=='y');
    }    
}
