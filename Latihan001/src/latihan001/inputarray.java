/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan001;

import java.util.Scanner;

/**
 *
 * @author Lab Informatika
 */
public class inputarray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Nama = ");
        String nama = scanner.nextLine();
        System.out.print("Jumlah = ");
        int jumlah = scanner.nextInt();
        int nilai[] = new int[jumlah];
        for (int i = 0; i < jumlah; i++) {
            int j=i+1;
            System.out.print("Nilai ke - "+j+" = ");
            nilai[i] = scanner.nextInt();
        }
        for (int i = 0; i < jumlah; i++) {
            int j=i+1;
            System.out.println("Nilai ke - "+j+" = "+nilai[i]);
        }
    }
}
